// @Author
// By C. Graamans
// Carapax.it Vital10 Protractor test
//

// Notes
//
// 1. Set env variables v10_TESTUSER_NAME and v10_TESTUSER_PWD
// windows: $env:v10_TESTUSER_NAME = ""
// 
// 2. Syntax guide here
// https://gist.github.com/javierarques/0c4c817d6c77b0877fda
//
// 3. 2-factor mail 
// https://www.npmjs.com/package/mail-listener2

let entrypoint = 'https://test-portal.mijnhep.nl/dashboard';
describe('Vital10 login', ()=>{

	beforeEach(()=>{

		browser.driver.manage().window().maximize();
	    browser.ignoreSynchronization = true;
			
		browser.get(entrypoint);
		browser.sleep(2000);

		element(by.css('#Email')).sendKeys(process.env.v10_TESTUSER_NAME);
		element(by.css('#Password')).sendKeys(process.env.v10_TESTUSER_PWD);

		element(by.buttonText('Inloggen')).click();

		browser.wait(()=>{
			return element(by.css('#wrapper > div.sidebar.left.bg-dark.font-white > div.panel-user.ng-trigger.ng-trigger-fadeIn > div')).isPresent();
		}, 5000);

	});

	afterEach(()=>{
		
		element(by.css('#wrapper > div.sidebar.left.bg-dark.font-white > div.panel-navmenu > ul > li:nth-child(8)')).click();

		browser.sleep(2000);

	    browser.ignoreSynchronization = false;


	});

	it('allows button to be clicked.', ()=>{

		browser.sleep(20000);

		// https://gist.github.com/javierarques/0c4c817d6c77b0877fda

	});

});