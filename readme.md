# Install

    npm install -g protractor
    webdriver-manager update

# Run

    webdriver-manager start
    protractor protractor.conf.js