exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['spec/**/*.js'],
  framework: 'jasmine2',
	onPrepare: function() {
		var jasmineReporters = require('jasmine-reporters');
		jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
		    consolidateAll: true,
		    savePath: 'testresults',
		    filePrefix: 'xmloutput'
		}));
	}
};